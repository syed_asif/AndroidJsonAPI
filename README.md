# AndroidJsonAPI

An API for retrieving json data in android
[![](https://jitpack.io/v/com.gitlab.syed_asif/AndroidJsonAPI.svg)](https://jitpack.io/#com.gitlab.syed_asif/AndroidJsonAPI)

## Running the tests

There is a project for testing the api. Project link: https://gitlab.com/syed_asif/TestJsonAPI

## Built With

* **Android Studio - 3.1.x**

## Use the API in your project

Edit your project's build.gradle file and add the following,

    allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
	
Now edit your app's build.gradle file and add the following dependency,

    dependencies {
	        implementation 'com.gitlab.syed_asif:AndroidJsonAPI:v1.0.0'
	}


## License

    Copyright 2018 Syed Asif Mahmood
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## Acknowledgments

The tutorial that was followed to develop the api: https://www.androidbegin.com/tutorial/android-json-parse-images-and-texts-tutorial/

